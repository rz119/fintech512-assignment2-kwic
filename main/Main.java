import java.io.*;
import java.util.*;

public class Main {
    public static void main (String[] args){
        String str;
        Set<String> ignoredWords = new HashSet<>();
        List<String> list = new ArrayList<>();
        int flag = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter ignored words, ::, or titles:");
        try {
            while ((str = br.readLine()) != null){
                if (str.equals("::")){
                    flag = 1;
                    continue;
                }
                if (flag == 0){
                    ignoredWords.add(str);
                }
                else {
                    list.add(str);
                }
            }
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        Titles titles = new Titles(list);
        SortedMap<String, List<List<Integer>>> keywordMap =
                titles.constructKeywordsMap(ignoredWords);

        List<String> keywordTitles = titles.constructKeywordTitles(keywordMap);

        for (String keywordTitle : keywordTitles) {
            System.out.println(keywordTitle);
        }
    }
}
