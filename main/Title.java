import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

public class Title {
    private String title;
    private final int index;

    Title(String title, int index){
        this.index = index;
        this.title = title;
        this.title = this.title.toLowerCase();
    }

    public SortedMap<String, List<List<Integer>>> extractKeywords(
            SortedMap<String, List<List<Integer>>> keywordMap,
            Set<String> ignoredWords
    ){
        String[] words = title.split("\\s+"); // splits by whitespace
        for (int i=0; i<words.length; i++) {
            if (ignoredWords.contains(words[i])){
                continue;
            }
            if (keywordMap.containsKey(words[i])){
                List<Integer> pair = new ArrayList<>();
                pair.add(index);
                pair.add(i);
                keywordMap.get(words[i]).add(pair);
            }
            else{
                List<List<Integer>> value = new ArrayList<>();
                List<Integer> pair = new ArrayList<>();
                pair.add(index);
                pair.add(i);
                value.add(pair);
                keywordMap.put(words[i], value);
            }
        }
        return keywordMap;
    }
}
