import java.util.*;

public class Titles {
    private final List<String> titles;

    Titles(List<String> list){
        this.titles = list;
    }

    public SortedMap<String, List<List<Integer>>> constructKeywordsMap(Set<String> ignoredWords){
        SortedMap<String, List<List<Integer>>> keywordsMap = new TreeMap<>();
        for (int i=0; i<titles.size(); i++){
            Title title = new Title(titles.get(i), i);
            keywordsMap = title.extractKeywords(keywordsMap, ignoredWords);
        }
        return keywordsMap;
    }

    public List<String> constructKeywordTitles(SortedMap<String, List<List<Integer>>> keywordsMap){
        List<String> keywordTitles = new ArrayList<>();
        for (SortedMap.Entry<String, List<List<Integer>>> map : keywordsMap.entrySet()) {
            String keyword = map.getKey().toUpperCase();
            List<List<Integer>> value = map.getValue();
            for (List<Integer> integers : value) {
                int titleIndex = integers.get(0);
                int wordIndex = integers.get(1);
                String[] title = titles.get(titleIndex).toLowerCase().split(" ");
                title[wordIndex] = keyword;
                String delimiter = " ";
                keywordTitles.add(String.join(delimiter, title));
            }
        }
        return keywordTitles;
    }
}
