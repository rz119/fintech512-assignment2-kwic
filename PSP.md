# PSP for KWIC

## Problem description

Given a list of titles and a list of “words to ignore”, you are to write a program that generates a KWIC (Key Word In Context) index of the titles. In a KWIC-index, a title is listed once for each keyword that occurs in the title. The KWIC-index is alphabetized by keyword. Any word that is not one of the “words to ignore” is a potential keyword.

## Conceptual design


|          Titles          | Lines of code |
|:------------------------:|:-------------:|
|   store all the titles   |       3       |
| Give each title an index |       1       |
|  construct Keywords Map  |       5       |
|  construct KWIC Titles   |      12       |

|                       Title                        | Lines of code |
| :------------------------------------------------: | :-----------: |
| store keywords and the title index in a hash table |      20       |
|                                                    |               |

|                  Main                  | Lines of code |
|:--------------------------------------:|:-------------:|
|               Read input               |      10       |
| Store input in ignore words and titles |      20       |
|                                        |               |



## Planning

|                       Total                        | 150 mins |
| :------------------------------------------------: |:--------:|
|         store all the titles          | 20 mins  |
|                 Give each title an index                | 20 mins  |
|             construct Keywords Map              | 20 mins  |
|                    construct KWIC Titles               | 20 mins  |
| store keywords and the title index in a hash table | 20 mins  |
|                     Read input                     | 20 mins  |
|       Store input in ignore words and titles       | 30 mins  |

## Development

|                                              Issues                                              |                                                                                                       |
|:------------------------------------------------------------------------------------------------:|-------------------------------------------------------------------------------------------------------|
| To avoid iterate titles several times, use nested list to store title index and keyword location |
|                                   Convert String[] and String                                    |      |
|                                  Handle lowercase and uppercase                                  |      |
|                                                                                                  |                                                                                                       |

|                  Development Time                  | Total: 142 mins |
| :------------------------------------------------: |-----------------|
|         store all the titles          | 1 min           |
|                 Give each title an index                | 1 min           |
|             construct Keywords Map              | 30 mins         |
|                    construct KWIC Titles               | 10 mins         |
| store keywords and the title index in a hash table | 30 mins         |
|                     Read input                     | 60 mins         |
|       Store input in ignore words and titles       | 10 mins         |


|          Titles          | Lines of code |
|:------------------------:|:-------------:|
|   store all the titles   |       3       |
| Give each title an index |       1       |
|  construct Keywords Map  |       5       |
|  construct KWIC Titles   |      12       |

|                       Title                        | Lines of code |
| :------------------------------------------------: | :-----------: |
| store keywords and the title index in a hash table |      20       |
|                                                    |               |

|                  Main                  | Lines of code |
|:--------------------------------------:|:-------------:|
|               Read input               |      10       |
| Store input in ignore words and titles |      20       |
|                                        |               |

## Testing

|           Defect           | Testing Time |
|:--------------------------:|--------------|
|    testTitle No defect     | 10 mins      |
|    testTitles No Defect    | 10 mins      |
| testKeywordTitle No Defect | 20 mins      |
|                            |              |
|                            |              |
|                            |              |
|                            |              |

## Evaluation

- Total time: 142 mins
- Notable issues:
  - Use sorted hashmap to handle the alphabetical sorting of titles
  - Use set to store ignored words







