import org.junit.Assert;
import org.junit.Test;
import java.util.*;

public class TestKWIC {
    @Test
    public void testTitle(){
        Title title = new Title("A Portrait of The Artist As a Young Man", 0);
        SortedMap<String, List<List<Integer>>> keywordMap = new TreeMap<>();
        Set<String> ignoredWords = new HashSet<>();
        ignoredWords.add("a");
        ignoredWords.add("of");
        ignoredWords.add("the");
        ignoredWords.add("as");

        keywordMap = title.extractKeywords(keywordMap, ignoredWords);
        for (SortedMap.Entry<String, List<List<Integer>>> set :
                keywordMap.entrySet()) {
            // Printing all elements of a Map
            System.out.println(set.getKey() + " = "
                    + set.getValue());
        }
    }

    @Test
    public void testTitles(){
        Set<String> ignoredWords = new HashSet<>();
        ignoredWords.add("a");
        ignoredWords.add("of");
        ignoredWords.add("the");
        ignoredWords.add("as");
        ignoredWords.add("is");
        ignoredWords.add("and");
        ignoredWords.add("but");

        List<String> list = new ArrayList<>();
        list.add("Descent of Man");
        list.add("The Ascent of Man");
        list.add("The Old Man and The Sea");
        list.add("A Portrait of The Artist As a Young Man");
        list.add("A Man is a Man but Bubblesort IS A DOG");
        Titles titles = new Titles(list);

        SortedMap<String, List<List<Integer>>> keywordMap =
                titles.constructKeywordsMap(ignoredWords);

        for (SortedMap.Entry<String, List<List<Integer>>> set :
                keywordMap.entrySet()) {
            // Printing all elements of a Map
            System.out.println(set.getKey() + " = "
                    + set.getValue());
        }
    }

    @Test
    public void testKeywordTitle(){
        Set<String> ignoredWords = new HashSet<>();
        ignoredWords.add("a");
        ignoredWords.add("of");
        ignoredWords.add("the");
        ignoredWords.add("as");
        ignoredWords.add("is");
        ignoredWords.add("and");
        ignoredWords.add("but");

        List<String> list = new ArrayList<>();
        list.add("Descent of Man");
        list.add("The Ascent of Man");
        list.add("The Old Man and The Sea");
        list.add("A Portrait of The Artist As a Young Man");
        list.add("A Man is a Man but Bubblesort IS A DOG");

        Titles titles = new Titles(list);

        SortedMap<String, List<List<Integer>>> keywordMap =
                titles.constructKeywordsMap(ignoredWords);

        List<String> keywordTitles = titles.constructKeywordTitles(keywordMap);

        for (String keywordTitle : keywordTitles) {
            System.out.println(keywordTitle);
        }
    }
}
